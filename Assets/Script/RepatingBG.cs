﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepatingBG : MonoBehaviour {

    //vertical sprite size in pixels
    //***for proper operetion, the height of the sprite must be great than height of the camera
    //***You can use BOX collide 
    public float vertical_size;
    //sprit up offset
    public Vector2 _offSet_Up;

    void Update()
    {
        //if the sprite is component gone
        if (transform.position.y < -vertical_size)
            //call the RepeatBackground
            RepeatBackground();

    }

    //Metod RepeatBackground
    //Move two sprite one after the other, creating an end endless background
    void RepeatBackground()
    {
        //Set the offset twice the  height of the sprite
        _offSet_Up = new Vector2(0, vertical_size * 2);
        //Set a new position  for  the sprite
        transform.position = (Vector2)transform.position + _offSet_Up;
    }
    
}
