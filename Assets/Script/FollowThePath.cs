﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowThePath : MonoBehaviour {

    //An array of waypoint along which the enemy moves in a wave
   [HideInInspector] public Transform[] path_Points;
    //The speed at which the enemy moves
    [HideInInspector]public float speed_Enemy;
    //Destroy the surviring enemys at the end of the path send then to the beginner of the path
   [HideInInspector] public bool is_retern;

    //Store vector3 of all waypoint 
   [HideInInspector] public Vector3[] _new_Position;

    private int cur_POS;

	// Use this for initialization
	void Start ()
    {
        _new_Position = NewPositionByPath(path_Points);
        //Send the  current opponent to the starting point
        transform.position = _new_Position[0];
		
	}

    void Update()
    {
        //Move the current enemy  to the points of the path at a given speed
        transform.position = Vector3.MoveTowards(transform.position, _new_Position[cur_POS],
                                                 speed_Enemy * Time.deltaTime);
        //if the current enemy has reached the point of the path
        if (Vector3.Distance(transform.position, _new_Position[cur_POS]) < 0.2f)
        {
            //Set the waypoitn
            cur_POS++;

            // if the current enemy reaches the last point
            if (is_retern && Vector3.Distance(transform.position,
                                             _new_Position[_new_Position.Length - 1]) < 0.3f)
            
                cur_POS = 0;
            
        }
        // if the curent enemy reaches the last point
        if (Vector3.Distance(transform.position, _new_Position[_new_Position.Length - 1]) < 0.2f && !is_retern)
        {
            Destroy(gameObject);
        }



    }
    
    Vector3[] NewPositionByPath (Transform[] pathPos)
    {
        Vector3[] pathPosition = new Vector3[pathPos.Length];

        for (int i = 0; i<path_Points.Length; i++)
        {
            pathPosition[i] = pathPos[i].position;

        }
        pathPosition = Smoothing(pathPosition);
        pathPosition = Smoothing(pathPosition);
        pathPosition = Smoothing(pathPosition);
        return pathPosition;
    }
    Vector3[] Smoothing(Vector3[] path_Position)
    {
        Vector3[] new_Path_Position = new Vector3[(path_Position.Length - 2) * 2 + 2];
        new_Path_Position[0] = path_Position[0];
        new_Path_Position[new_Path_Position.Length - 1] = path_Position[path_Position.Length - 1];

        int j = 1;
        for (int i = 0; i < path_Position.Length - 2; i++)
        {
            new_Path_Position[j] = path_Position[i] + (path_Position[i + 1] - path_Position[i]) * 0.75f;
            new_Path_Position[j + 1] = path_Position[i + 1] + (path_Position[i + 2] - path_Position[i + 1]) * 0.25f;
            j += 2;
        }
        return new_Path_Position;
    }


}
