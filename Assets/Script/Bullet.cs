﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    //The damage inflicted by each bullet
    public int damage;
    //Is the bullet of an enemy or player
    public bool is_Enemy_Bullet;

    //Method destriction bullet 
    private void Destriction()
    {
        Destroy(gameObject);
    }

    private void  OnTriggerEnter2D(Collider2D coll)
    {
        //If the bullet belong  to an enemy and collider with a player
        if(is_Enemy_Bullet && coll.tag=="Player")
        {
            //Call the Player for the method of taking damage and deal damage to him
            Player.instace.GetDamaga(damage);
            //Dectriction bullit
            Destriction();
        }
        //if the bullent belong to the player and collider with the enemy
        else if (is_Enemy_Bullet && coll.tag=="Enemy")
        {
            //At the collider we find Enemy component and call the method for taking damage
            coll.GetComponent<Enemy>().GetDamag(damage);
            //Destriction bullit
            Destriction();

        }

        //if the bullet belong to the enemy and collider with the player's shild 
        else if (is_Enemy_Bullet && coll.tag == "Shield")
        {
            //Call the Player for the metod of taking damage shield and deal damage to him 
            Player.instace.GetDamageShild(damage);
            //Destriction bullet 
            Destriction();

        }

    }


	
}
