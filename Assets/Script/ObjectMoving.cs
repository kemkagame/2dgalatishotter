﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMoving : MonoBehaviour {

    // the speed at which the object moves 
    public float speed;

	
	// Update is called once per frame
	void Update ()
    {
        //Move  the object vertical with a given speed
        transform.Translate(Vector3.up * speed * Time.deltaTime);
		
	}
}
