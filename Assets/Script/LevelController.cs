﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


[System.Serializable]
public class EnemyWaves
{
    //Time when the wave will apper on the stage
    public float timeToStarte;
    //The Enemy Wave prefab to be spawned
    public GameObject wave;
    //this wave after which the game will end
    public bool is_Last_Wave;

   


}
public class LevelController : MonoBehaviour {

    //Static referents to the LevelController 
    public static LevelController instance;
    //Array of player ship
    public GameObject[] player_Ship;
    //Referents to the EnemyWaves
    public EnemyWaves[] enemeWaves;
    //the finish game or nor
    private bool is_Final = false;

    //Pause Menu
    public GameObject panel;
    //Show and hide pause menu
    private bool is_Pause;
    //Btn pause menu (Exit, Retern, Restart)
    public GameObject[] btnPause;


    private void Awake ()
    {
        //Setting up the reference
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        //Create all enemy waves 
        for (int i = 0; i < enemeWaves.Length; i++)
        {
            //Start CreateEnemeWave as a corotine
            StartCoroutine(CreateEnemeWave(enemeWaves[i].timeToStarte, enemeWaves[i].wave, enemeWaves[i].is_Last_Wave));
        }
    }
    private void Update()
    {
        //if is_Final = true there are  no object with the Enemy tag left and the pause button is not activity      
        if(is_Final == true && GameObject.FindGameObjectsWithTag("Enemy").Length == 0 && !is_Pause)
        {
            //You win game 
            Debug.Log("Win");
            GamePause();
            btnPause[1].SetActive(false);
        }

        if(Player.instace == null && !is_Pause)
        {
            //Game over
            Debug.Log("GameOver");
            GamePause();

        }
    }

    public void GamePause()
    {
        //Call pause pane
        if (!is_Pause)
        {
            is_Pause = true;
            //time in the game is frozen
            Time.timeScale = 0;
            //Show  panel
            panel.SetActive(true);
            //if the player is not dead, the retern  button instead of the restart button
            if (Player.instace != null)
            {
                btnPause[0].SetActive(false);
                btnPause[1].SetActive(true);


            }
            //if the palayer is dead the restart button instead of the retern button
            else
            {
                btnPause[0].SetActive(true);
                btnPause[1].SetActive(false);
            }
        }
        // Hide pause panel
        else
        {
            is_Pause = false;
            Time.timeScale = 1;
            panel.SetActive(false);

        
        }
    }

   public  void BtnRestartGame()
    {
        Time.timeScale = 1;
        //load current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    IEnumerator CreateEnemeWave (float delay, GameObject Wave, bool Final)
    {
        //if creation wave time ! = 0 and player  is alive
        if (delay != 0)
            yield return new WaitForSeconds(delay);
        if(Player.instace !=null)
            Instantiate(Wave); 
        
        //if this wave is the last 
        if(Final==true)
            is_Final = true;
        

    }

	
}
