﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour {
    //The Bonus prefab  to be spawned
    public GameObject obj_Bonus;
    //How long  betwen each Bonus spawn
    public float time_Bonus_Spawn;
    //an array the planets prefab to be spawner
    public GameObject[] obj_Planet;
    //How long betwen each planet spawn 
    public float time_Planet_Spawn;
    //The speed at which the Planet moves
    public float speed_Planet;
    //Planet list 
    List<GameObject> planetslist = new List<GameObject>();

    private void Start()
    {
        //Start function PowerBonusCreat as a corountine
        StartCoroutine(BonusCreation());
        //Start function PlanetCreation as a corontino
        StartCoroutine(PlanetCreation());
    }
    IEnumerator BonusCreation()
    {
        //Creation Bonus
        while (true)
        {
            //Every time_Bonus_Spawn seconds 
            yield return new WaitForSeconds(time_Bonus_Spawn);
            //Creatie an instanti  of the prefab Bonus
            //The bonus will be cread above the camera visibitity
            Instantiate(obj_Bonus, new Vector2(Random.Range(PlayerMoving.instanse._boarder.minX,
                PlayerMoving.instanse._boarder.maxX),
                PlayerMoving.instanse._boarder.maxY * 1.5f), Quaternion.identity);
        }

    }

    IEnumerator PlanetCreation()
    {
        //Fill the list with planet
        for (int i = 0; i<obj_Planet.Length; i++)
        {
            planetslist.Add(obj_Planet[i]);
        }

        yield return new WaitForSeconds(7);
        //Creation planet
        while(true)
        {
            //select a random planet from the list
            int randomIndex = Random.Range(0, planetslist.Count);
            //Creat an instanse of the planet taking  into account the limit of the player
            GameObject newPlanet = Instantiate(planetslist[randomIndex],
                new Vector2(Random.Range(PlayerMoving.instanse._boarder.minX, PlayerMoving.instanse._boarder.maxX),
                PlayerMoving.instanse._boarder.maxY * 2f),
                Quaternion.Euler(0,0, Random.Range(-25,25)));

            //REmove the selected planet from the list 
            planetslist.RemoveAt(randomIndex);
            //if  the list is emty fill it agein 
            if (planetslist.Count == 0)
            {
                    for (int i = 0; i< obj_Planet.Length; i++)
                {
                    planetslist.Add(obj_Planet[i]);
                }
            }
            //on the created planet we find the Component movingobject and  set the speed of moving
            newPlanet.GetComponent<ObjectMoving>().speed = speed_Planet;
            yield return new WaitForSeconds(time_Planet_Spawn);
        }
    }
}
