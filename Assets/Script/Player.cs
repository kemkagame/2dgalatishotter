﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    //Static reference to the Player (can be used in other scripts)
    public static Player instace = null;
    //Player health
    public int player_health;

    //Reference to the Shield GameObject
    public GameObject obj_Shield;
    //Shild health
    public int shild_health =1;

    //Referent private to the UI's health bar
    private Slider _slider_HP;
    //Referent private to the Ui's shild bar
    private Slider _slide_shild_HP;

	// Use this for initialization
	void Awake ()
    {
        //Setting up the referense
        if(instace == null)
        {
            instace = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //Look for an object with tag Hp_Player and take the Slider component from it
        _slider_HP = GameObject.FindGameObjectWithTag("sl_HP").GetComponent<Slider>();
        _slide_shild_HP = GameObject.FindGameObjectWithTag("sl_Shild").GetComponent<Slider>();

		
	}
    private void Start()
    {
        //Set the health bar's value to the current health 
        _slider_HP.value = (float)player_health / 10; // slider has a rang from 0 to 1
        //If the shield has life
        if(shild_health !=0)
        {
            //Show shield 
            obj_Shield.SetActive(true) ;
            //Set the shiled bar's vaue to the current heath
            _slide_shild_HP.value = (float)shild_health / 10; // slider has a rang from 0 to 1
        }
        else
        {
            obj_Shield.SetActive(false);
            _slide_shild_HP.value = 0;
        }
    }

    // Method of taking damage shild 
    public void GetDamageShild (int damaga)
    {
        //reduce the shild health by the damage amount
        shild_health -= damaga;
        //Update shild bae's value to the current shild heath
        _slide_shild_HP.value = (float)shild_health / 10;


        //if the shild does not have a helth
        if (shild_health <= 0)
        {
            //Hide shild 
            obj_Shield.SetActive(false);
        }

    }

    public void GetDamaga(int damaga)
    {
        //Reduce the health by the damaga amaunt
        player_health -= damaga;
        //Update Player heath bar value to the current hp player
        _slider_HP.value = (float)player_health / 10;
        //if the player does not have a health 
        if(player_health<=0)
        {
            //call the player destruction
            Destruction();
        }
    }

    void Destruction()
    {
        //destroy the current player object
        Destroy(gameObject);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
