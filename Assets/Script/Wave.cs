﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShootingSettings
{
    //Chance of a shot enemy in the from of a slider
    [Range(0, 100)]
    public int shot_Chanse;
    //time interval withit which the shot occurs 
    public float shot_Time_Min, shot_Time_Max;
}
public class Wave : MonoBehaviour {
    //Referents to the SootingSettings
    public ShootingSettings shotting_Setings;

    [Space]
    //The enemy prefab to be spawned 
    public GameObject obj_Enemy;
    //amaunt of enemis in one wave
    public int count_in_Wave;
    //The speed at which the enemy moves 
    public float speed_Enemy;
    //Time between spawn enemy in wave 
    public float time_Spawn;
    //An array waypoints along which the enemy moves in a wave
    public Transform[] path_Points;
    //Destroy the surviving enemis at the end of the path or send them to beginneng of the path
    public bool is_return;


    //Test Wave
    //an  infinite wave appears every 5 second to debag the wave
    [Header("Test wave!")]

    public bool is_Test_Wave;

    private FollowThePath follow_Component;
    private Enemy enemy_Component_Scripts;

    private void Start()
    {
        //Start function CreateEnemyWave as a coroutine
        StartCoroutine(CreateEnemyWave());
    }

    IEnumerator CreateEnemyWave()
    {
        //Create enemies 
        for (int i = 0; i < count_in_Wave; i++)
        {
            //Create an instance of the prefab obj Enemy in the obj_Enemy position
            GameObject new_enemy = Instantiate(obj_Enemy, obj_Enemy.transform.position, Quaternion.identity);

            //Try and finde an FollowThePath script on the gameobject new_enemy
            follow_Component = new_enemy.GetComponent<FollowThePath>();
            //Specify the path that will move the new_enemy
            follow_Component.path_Points = path_Points;
            //Specify the speed with which the new enemy will move 
            follow_Component.speed_Enemy = speed_Enemy;
            //Destroy the surviving enemies at the end of the path or send them to the beginning of the path
            follow_Component.is_retern = is_return;

            //try and find an Enemy script on the gameobject new_enemy
            enemy_Component_Scripts = new_enemy.GetComponent<Enemy>();
            // Specifi shot chance a new enemy
            enemy_Component_Scripts.shot_Chance = shotting_Setings.shot_Chanse;
            //Specifi time_interval with wich the shot occurs
            enemy_Component_Scripts.shot_Time_min = shotting_Setings.shot_Time_Min;
            enemy_Component_Scripts.shot_Time_max = shotting_Setings.shot_Time_Max;


            new_enemy.SetActive(true);
            //Every time_Spawn second 
            yield return new WaitForSeconds(time_Spawn);


        }
        //IF  wave test 
        if(is_Test_Wave)
        {
            yield return new WaitForSeconds(5f);
            StartCoroutine(CreateEnemyWave());
        }
        //if is_return =false detroy the enemy at the end of the path
        if (!is_return)
        {
            Destroy(gameObject);
        }
    }

    //To make it eaies to set up enemy waypoints, connect them with a line
    void OnDrawGizmos()
    {
        NewPositionByPath(path_Points);
    }
    void NewPositionByPath(Transform[] path)
    {
        Vector3[] path_Position = new Vector3[path.Length];
        for(int i = 0; i < path.Length; i++)
        {
            path_Position[i] = path[i].position;
        }
        path_Position = Smoothing(path_Position);
        path_Position = Smoothing(path_Position);
        path_Position = Smoothing(path_Position);
        for (int i = 0; i < path_Position.Length -1; i++)
        {
            Gizmos.DrawLine(path_Position[i], path_Position[i + 1]);
        }

    }
    Vector3[] Smoothing(Vector3[] path_Position)
    {
        Vector3[] new_Path_Position = new Vector3[(path_Position.Length - 2) * 2 + 2];
        new_Path_Position[0] = path_Position[0];
        new_Path_Position[new_Path_Position.Length - 1] = path_Position[path_Position.Length - 1];

        int j = 1;
        for(int i = 0; i < path_Position.Length-2; i++)
        {
            new_Path_Position[j] = path_Position[i] + (path_Position[i + 1] - path_Position[i]) * 0.75f;
            new_Path_Position[j + 1] = path_Position[i + 1] + (path_Position[i + 2] - path_Position[i + 1]) * 0.25f;
            j += 2;
        }
        return new_Path_Position;


    }
}
