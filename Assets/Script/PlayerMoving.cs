﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Serialaze allow you to ember a Board class in the inspector
[System.Serializable]
public class  Boarder
{
    //OffSet from the left border
    public float minX_offset = 1.1f;
    //offSet from the rigth border
    public float maxX_offset = 1.1f;
    //offSet from the bottom border
    public float minY_offset = 1.1f;
    //offset from the top border
    public float max_Yoffset = 1.1f;
    //HadeInInspector makes variboles that are not displayed in the inspector
    //These variable create bandaruis  that the player cannot leave
    [HideInInspector]
    public float minX, maxX, minY, maxY;
}
public class PlayerMoving : MonoBehaviour {

    //Static reference to the PlayerMoving 
    public static PlayerMoving instanse;
    //Referents to the boarder class
    public Boarder _boarder;
    //The speed witch the player moves
    public float speed = 5;
    //Reference private to the Camera
    private Camera _camera;
    //Save the 2D coordinat(xy)
    private Vector2 _mousePosition;
    

	void Awake()
    {
        //Setting up the reference
        if(instanse==null)
        {
            instanse = this;
        }
        else
        {
            Destroy(gameObject);
        }
        _camera = Camera.main;
    }
	
    private void Start()
    {
        //Call the Resize Boarde metod
        Resize();
    }
	// Update is called once per frame
	void Update ()
    {
		//if the mouse button is down
        if(Input.GetMouseButton(0))
        {
            //Get 2D coordinat(xy) mouse down
            _mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            _mousePosition.y += 1.5f;
            //Move our player to the 2d coordinat
            transform.position = Vector2.MoveTowards(transform.position, _mousePosition, 
                                                     speed * Time.deltaTime);
        }

        //if  a plaer iss trying  to go abroad, do not let him
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, _boarder.minX, _boarder.maxX),
                                         Mathf.Clamp(transform.position.y, _boarder.minY, _boarder.maxY));
	}
    //the calcuations  depens on the size of the camera 
    void Resize()
    {
        //border left
        _boarder.minX = _camera.ViewportToWorldPoint(Vector2.zero).x + _boarder.minX_offset;
        //boarder botton
        _boarder.minY = _camera.ViewportToWorldPoint(Vector2.zero).y + _boarder.minY_offset;
        //boarder right
        _boarder.maxX = _camera.ViewportToWorldPoint(Vector2.right).x - _boarder.maxX_offset;
        //boarder top
        _boarder.maxY = _camera.ViewportToWorldPoint(Vector2.up).y - _boarder.max_Yoffset;
    }
}
