﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour {

	private void OnTriggerEnter2D(Collider2D collision)
    {
        // If the entering collider is the player
        if (collision.tag == "Player")
        {
            if (PlayerShoting.instance.cur_Power_Level_Gun < PlayerShoting.instance.max_Power_Level_Guns)
            {
                //Increase the current power level of guns
                PlayerShoting.instance.cur_Power_Level_Gun++;
            }
            //Destroy the current Bonus object
            Destroy(gameObject);
        }
    }
}
