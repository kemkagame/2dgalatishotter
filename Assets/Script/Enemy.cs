﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {


    //Enemy Health
    public int Enemy_Health;

    [Space]
    //The Bullet prefab to the spawned
    public GameObject obj_Bullet;
    //time intervsl within the shot occurs
    public float shot_Time_min, shot_Time_max;
    //the probability of the enemis shot 
    public int shot_Chance;


    [Header("Boss")]
    //the current object is the boss
    public bool is_Boss;
    //The bullet prefab extra boss shot 
    public GameObject obj_Bullet_Boss;
    //How long between each bullet Boss spawn
    public float time_Bullet_Boss_Spawn;
    //Time to determine when to shoot
    private float _timer_shot_Boss;
    //the probability of the boss shot
    public int shot_Chance__Boss;


    private void Start()
    {
        //if the current object is not Boss, make only one shot
        if (is_Boss)
        {
            //Call the OpengFire in he ti,e interval bettwen shot_Time_min, and shot_Time_Max
            Invoke("OpenFire", Random.Range(shot_Time_min, shot_Time_max));
        }
    }

    private void Update()
    {
        //if the current is Boss
        if(is_Boss)
        {
            //everty timer_Shot_Boss second we call the metod
            //Openfire
            //OpenfireBoss
            if(Time.time > _timer_shot_Boss)
            {
                _timer_shot_Boss = Time.time + time_Bullet_Boss_Spawn;
                OpenFire();
                OpenFireBoss();

            }
        }
    }

    // Method OpenFireBoss
    private void OpenFireBoss()
    {
        //if random vaalue  less that shot chance, making a extra boss shot
        if(Random.value<(float)shot_Chance__Boss/100)
        {
            //Shot a few bullet like a fan
            for (int zZz = -40; zZz < 40; zZz+=10)
            {
                //Create an instance of the prefab obj_Bullet_Boss in the boss position and
                //rotates zZz degrees  around the z axis
                Instantiate(obj_Bullet_Boss, transform.position, Quaternion.Euler(0, 0, zZz));



            }
        }
    }

    //method OpenFire
    private void OpenFire ()
    {
        //if random value less than shot chance, making a shot
        if(Random.value < (float)shot_Chance / 100)
        {
            //Create an instatiete of the prefab obj Bullet in the enemy position 
            Instantiate(obj_Bullet, transform.position, Quaternion.identity);
        }
    }
    //Metoth of taking damage by the enemy
    public void GetDamag(int damag)
    {
        //Reduce the healt by the damage amount
        Enemy_Health -= damag;
        //if the enemy does not have a healt
        if(Enemy_Health<=0)
        {
            //call the enemy destruction metod
            Dectruction();
        }
    }
    void Dectruction()
    {
        //Destroy the current player object
        Destroy(gameObject);

    }
    //if enemy collider player, Player get thr damag 
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag == "Player")
        {
            GetDamag(1);
            Player.instace.GetDamaga(1);
        }
        if(coll.tag == "Shild")
        {
            GetDamag(1);
            Player.instace.GetDamageShild(1);
        }
    }
	
}
