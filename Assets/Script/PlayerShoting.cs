﻿
using System.Collections;

using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Guns
{
    public GameObject obj_Central_Gun,
        obj_Rigth_Gun,
        obj_Left_Gun;
    public ParticleSystem ps_center, ps_left, ps_rigth;
}
public class PlayerShoting : MonoBehaviour {

    //Static reference to the PlayerShoting (can be used in other script)
    public static PlayerShoting instance;
    //Reference to the Guns
    public Guns guns;
    //Maximum power level of guns
    [HideInInspector]
    public int max_Power_Level_Guns = 5;
    //The Bullet prefab to be spawed
    public GameObject obj_Bullet;
    //How long between each bullet spawn
    public float time_Bullet_Spawn = 0.3f;
    //Timer to determine when to shoot
    [HideInInspector]
    public float timer_Shot;
    //Current level of weapon strength through the slider
    [Range(1, 5)]
    public int cur_Power_Level_Gun = 1;

    private void Awake()
    {
        //Setting up the reference
        if(instance==null)
        {
            instance = this;

        }
        else{
            Destroy(gameObject);

        }
    }
    private void Start()
    {
        //Try and find an Partical System component on the gameobject gun
        guns.ps_center = guns.obj_Central_Gun.GetComponent<ParticleSystem>();
        guns.ps_left = guns.obj_Left_Gun.GetComponent<ParticleSystem>();
        guns.ps_rigth = guns.obj_Rigth_Gun.GetComponent<ParticleSystem>();
    }
    private void Update ()
    {
        //ever timer_Shot_second we call the metod MakeAShot
        if(Time.time > timer_Shot)
        {
            timer_Shot = Time.time + time_Bullet_Spawn;
            //call the metod MakeAshot
            MakeAShot();
        }
    }
    private void CreateBullet (GameObject bullet, Vector3 position_bullet, Vector3 rotation_Bullet)
    {
        Instantiate(bullet, position_bullet, Quaternion.Euler(rotation_Bullet));
    }
    //Method MakeAShot
    //This method, depending on the cur_Power_Level_Guns, changes the player wepons shooting
    private void MakeAShot()
    {
        switch(cur_Power_Level_Gun)
        {
            //one gun shot
            case 1:
                CreateBullet(obj_Bullet, guns.obj_Central_Gun.transform.position, Vector3.zero);
                guns.ps_center.Play();
                break;
            //two gun shot
            case 2:
                CreateBullet(obj_Bullet, guns.obj_Rigth_Gun.transform.position, Vector3.zero);
                CreateBullet(obj_Bullet, guns.obj_Left_Gun.transform.position, Vector3.zero);
                guns.ps_rigth.Play();
                guns.ps_left.Play();

                break;
            //three gun shot
            case 3:
                CreateBullet(obj_Bullet, guns.obj_Central_Gun.transform.position, Vector3.zero);
                CreateBullet(obj_Bullet, guns.obj_Rigth_Gun.transform.position, new Vector3 (0,0,-5));
                CreateBullet(obj_Bullet, guns.obj_Left_Gun.transform.position, new Vector3 (0,0,5));
                guns.ps_center.Play();
                guns.ps_rigth.Play();
                guns.ps_left.Play();
                break;
            //fire guns shot
            case 4:
                CreateBullet(obj_Bullet, guns.obj_Central_Gun.transform.position, Vector3.zero);
                CreateBullet(obj_Bullet, guns.obj_Rigth_Gun.transform.position, new Vector3(0, 0, 0));
                CreateBullet(obj_Bullet, guns.obj_Rigth_Gun.transform.position, new Vector3(0, 0, 5));
                CreateBullet(obj_Bullet, guns.obj_Left_Gun.transform.position, new Vector3(0, 0, 0));
                CreateBullet(obj_Bullet, guns.obj_Left_Gun.transform.position, new Vector3(0, 0, -5));
                guns.ps_center.Play();
                guns.ps_rigth.Play();
                guns.ps_left.Play();
                break;
            //five guns shot
            case 5:
                CreateBullet(obj_Bullet, guns.obj_Central_Gun.transform.position, Vector3.zero);
                CreateBullet(obj_Bullet, guns.obj_Rigth_Gun.transform.position, new Vector3(0, 0, -5));
                CreateBullet(obj_Bullet, guns.obj_Rigth_Gun.transform.position, new Vector3(0, 0, -15));
                CreateBullet(obj_Bullet, guns.obj_Left_Gun.transform.position, new Vector3(0, 0, 5));
                CreateBullet(obj_Bullet, guns.obj_Left_Gun.transform.position, new Vector3(0, 0, 15));
                guns.ps_center.Play();
                guns.ps_rigth.Play();
                guns.ps_left.Play();
                break;
        }
    }

        
}
